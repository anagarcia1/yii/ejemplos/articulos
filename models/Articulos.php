<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "articulos".
 *
 * @property int $id
 * @property string|null $nombre
 * @property int|null $categoria
 * @property float|null $precio
 * @property int|null $stock
 * @property string|null $fecha
 */
class Articulos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'articulos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['categoria', 'stock'], 'integer'],
            [['precio'], 'number'],
            [['fecha'], 'safe'],
            [['nombre'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() 
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'categoria' => 'Categoria',
            'precio' => 'Precio',
            'stock' => 'Stock',
            'fecha' => 'Fecha',
        ];
    }
}
