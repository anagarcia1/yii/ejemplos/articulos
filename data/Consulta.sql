# elimiar la base de datos
DROP DATABASE IF EXISTS articulos;

/*
Crear y seleccionar 
la base de datos
*/

CREATE DATABASE articulos;

USE articulos; -- selecionar B de DD

CREATE TABLE articulos (
	id INT AUTO_INCREMENT,
	nombre VARCHAR(200),
	categoria INT, 
	precio FLOAT,
	stock BOOL,
	fecha DATE,
	PRIMARY KEY(id)	
);

CREATE TABLE categorias (
	id INT AUTO_INCREMENT,
	nombre VARCHAR(200),
	descripcion VARCHAR(500),
	numero INT, 
	PRIMARY KEY(id)	
);